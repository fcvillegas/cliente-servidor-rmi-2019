package uy.edu.cei.dda.rmi.common.server;

import java.rmi.Remote;
import java.rmi.RemoteException;

public interface ServerApp extends Remote {

	public void sayHello(String name) throws RemoteException;

}
