package uy.edu.cei.dda.rmi.server;

import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;

import uy.edu.cei.dda.rmi.common.server.ServerApp;

/**
 * Hello world!
 *
 */
public class App {
	public static void main(String[] args) throws RemoteException {
		ServerApp server = new ServerAppImpl();
		LocateRegistry.createRegistry(1099);
		ServerApp app = (ServerApp) UnicastRemoteObject.exportObject(server, 0);
		Registry registry = LocateRegistry.getRegistry();
		registry.rebind("server", app);
	}
}
