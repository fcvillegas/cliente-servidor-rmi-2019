package uy.edu.cei.dda.rmi.server;

import java.io.Serializable;
import java.rmi.RemoteException;

import uy.edu.cei.dda.rmi.common.server.ServerApp;

public class ServerAppImpl implements ServerApp, Serializable {

	protected ServerAppImpl() throws RemoteException {
		super();
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Override
	public void sayHello(String name) throws RemoteException {
		System.out.println("Hello " + name);
	}
	
}
