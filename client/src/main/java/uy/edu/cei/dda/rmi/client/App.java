package uy.edu.cei.dda.rmi.client;

import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

import uy.edu.cei.dda.rmi.common.server.ServerApp;

/**
 * Hello world!
 *
 */
public class App {
	public static void main(String[] args) throws Exception {
        Registry registry = LocateRegistry.getRegistry(1099);
        ServerApp server = (ServerApp) registry.lookup("server");
        server.sayHello("yo");
        while(true) {
        	Thread.sleep(100);
        }
	}
}
